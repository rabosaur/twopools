---
title: West-My-Friend
date: 2018-02-26
featuredImage: images/westmyfriend.jpg
categories: []
tags: []
author: ra

---
At: Two Pools in Meadowbrook (NE Seattle) - address on RSVP

West My Friend took the FAR-West regional Folk Alliance conference by storm last fall! This was the band we couldn't stop following around between showcase rooms in the wee hours, we just had to hear more! Eden, Jeff, Alex and Nick are all brilliant musicians, their original material is touching and catchy (i mean, really, really catchy!), their arrangements are clever and charming and rich and oh so precise... you do not want to miss this opportunity to see them up close and acoustic in our living room!
<!--more-->

We'll open the doors at 7 for snacks and mingling, music starts at 7:30. Messenger us to RSVP and get the address, or email sfsmarn [at] gmail [dot] com. 

Spread the word! We were super lucky to get this opportunity on short notice, and as many folks as possible should get a chance to have this awesome musical experience!


Event link: **<a href="https://www.facebook.com/events/194069411353500/">West My Friend</a>**
