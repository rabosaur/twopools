---
title: Jaspar Lepak
date: 2018-03-05
featuredImage: images/jasperlepak.jpg

---
<h4>SATURDAY APRIL 21 at 7:30 PM - <br><a href="https://form.jotform.com/80888290635165?whichShow=jaspar-lepak">***Click For seat reservations***</a></h4>

Singer-Songwriter JASPAR LEPAK
(**<a href="http://www.jasparlepak.com">www.jasperlepak.com</a>**)

Jaspar Lepak is a folk/americana songwriter whose remarkable gift for melody is only surpassed by her poetry. A child of the Sonoran Desert, her songs are deeply influenced by the many places she has called home: Tucson, Minneapolis, Durban, and currently Seattle. Sweeping across landscapes with an emotional depth that is extraordinary, her lyrics expose a brave vulnerability while her clear, pure voice touches the heart.
<!--more-->

"Jaspar Lepak's crystalline voice and lyrical phrasing is a wonder, a beacon of true musicality. Listening to her evocative songs sung with that gorgeous voice, I am reminded of how I felt as a young man, the first time I heard Joni Mitchell. Jaspar Lepak is a special talent whose presentation transcends age, styles and fads." <br>~ Tom May, River City Folk

