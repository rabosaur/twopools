#!/bin/bash
echo $1
if [ "$1" != "" ]; then
    hugo new article/$1.md
    code ./content/article/$1.md
else
    echo "You must specify an article name"
fi 
