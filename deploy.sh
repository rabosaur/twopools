#!/bin/sh
BUCKET_NAME=twopools.org
DISTRIBUTION_ID=E2BH2P859FXB2H


if [ ! -f ./config.toml ]; then
	echo "hugo site config.toml is NOT found, can't continue"
	echo "Perhaps this is running in the wrong directory?"
	exit 1
fi
if aws s3api head-bucket --bucket "$BUCKET_NAME" 2>/dev/null; then
	echo "S3 bucket $BUCKET_NAME already exists"
else
	echo "S3 bucket $BUCKET_NAME DOES NOT EXIST, CREATING IT"
	aws s3 mb s3://$BUCKET_NAME
	
fi

echo "regenerate the public folder"

if [ -d "./public" ]; then
    rm -rf ./public
fi
hugo -v

echo "fix up links within the site, this is a hugo bug on AWS S3 sites"

python ../hugo-s3-fixer/fixer.py -p ./public
if [ $? -ne 0 ]; then
   exit
fi

# ---
echo "Create website config"

echo "{
    \"IndexDocument\": {
        \"Suffix\": \"index.html\"
    },
    \"ErrorDocument\": {
        \"Key\": \"404.html\"
    },
    \"RoutingRules\": [
        {
            \"Redirect\": {
                \"ReplaceKeyWith\": \"index.html\"
            },
            \"Condition\": {
                \"KeyPrefixEquals\": \"/\"
            }
        }
    ]
}" > website.json

aws s3api put-bucket-website --bucket $BUCKET_NAME --website-configuration file://website.json
rm website.json


echo "copy up to AWS s3 bucket"
aws s3 sync --acl "public-read" --sse "AES256" public/ s3://$BUCKET_NAME --exclude 'post'
echo "Create invalidation of cloudfront"
aws cloudfront create-invalidation --distribution-id $DISTRIBUTION_ID --paths '/*'

echo "--------------------------------------------------"
aws s3 ls s3://$BUCKET_NAME --human-readable --recursive --summarize
#echo "--------------------------------------------------"
#aws s3 ls s3://twopools-logging --summarize --human-readable

echo "Done!"

